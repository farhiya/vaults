# Vault introduction
Vaults is an open source(ish) is a Hashicorp solution for secrets managment. This repo tells us how to install and execute vault commands.

## Main Problems Vault aims to solve

- Secrets sprawl
- Individual keys
- Organisation of secrets
- Using several keys can compromise several systems
- Encrypts the keys

## Installation commands

`brew tap hashicorp/tap`
`brew install hashicorp/tap/vault`
`brew upgrade hashicorp/tap/vault`

## Start vault
`vault server -dev`

In a new terminal/tab:
`export VAULT_ADDR='http://127.0.0.1:8200'`
`export VAULT_TOKEN='<root token given on first terminal>'`

## CRUD kv vault

```bash
# syntax
# vault <secret_engine> <action> <path> <arguments>
$ vault kv put secret/fj_id id=123
$ vault kv get secret/fj_id
$ vault kv delete secret/fj_id
$ vault kv get -format=json secret/fj_id
$ vault kv get -format=json secret/fj_id | jq
$ vault kv get -format=json secret/fj_id | jq .data.data.id
```

## Creating Dynamic Vaults


Create a dynamic vault:
vault secrets enable -path=aws_generator aws

Set the key:
Don't do this in production, use IAM roles

```bash
vault write aws_generator/config/root \
    access_key=$AWS_ACCESS_KEY_ID \
    secret_key=$AWS_SECRET_ACCESS_KEY \
    region=eu-west-1
```

Create a role:

```bash

vault write aws_generator/roles/my-role \
        credential_type=iam_user \
        policy_document=-<<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1426528957000",
      "Effect": "Allow",
      "Action": [
        "ec2:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
```

Generate the secret:
```bash 
$ vault read aws_generator/creds/my-role
```

Create a policy: 




## Links

Tutorial: https://learn.hashicorp.com/tutorials/vault/getting-started-install